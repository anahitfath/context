import React,{useState} from 'react';

export const UserContext=React.createContext();

function UserProvider(props) {

    const [name,setName]=useState("Anahit");
  return (
    <UserContext.Provider value={name}>
      {props.children}
    </UserContext.Provider>
  );
}


export default UserProvider;
