import React from 'react';
import UserProvider from './UserProvider';
import User from './User';

function App() {
  return <UserProvider>
  <User/>
  </UserProvider>;
}
export default App;


